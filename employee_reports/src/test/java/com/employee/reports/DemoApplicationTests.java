package com.employee.reports;

import com.employee.reports.controller.EmployeeApi;
import com.employee.reports.repositories.HolidaysRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigureMockMvc
@SpringBootTest
class DemoApplicationTests {
	@Autowired
	MockMvc mockMvc;

	@Test
	void testToGetMonthlyReport() throws Exception{
		MockHttpServletRequestBuilder request= MockMvcRequestBuilders.get("/monthly/{id}",3);
		MvcResult result=mockMvc.perform(request).andReturn();
		MockHttpServletResponse response=result.getResponse();
		assertEquals(HttpStatus.OK.value(),response.getStatus());
	}
	@Test
	void testToGetyearlyReports() throws Exception {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/monthly/{id}", 2022);
		MvcResult result = mockMvc.perform(request).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

}
