package com.employee.reports.models;

import javax.persistence.Column;
import java.time.LocalDate;
import java.time.LocalTime;

public class AttendanceModel {
    private Integer id;
    private Integer eid;
    private LocalDate date;
    private String task_description;
    private LocalTime check_In;
    private LocalTime check_out;
    private Boolean halfDay;
    public AttendanceModel() {
    }

    public AttendanceModel(Integer id, Integer eid, LocalDate date, String task_description, LocalTime check_In, LocalTime check_out, Boolean halfDay) {
        this.id = id;
        this.eid = eid;
        this.date = date;
        this.task_description = task_description;
        this.check_In = check_In;
        this.check_out = check_out;
        this.halfDay = halfDay;
    }

    public Integer getId() {
        return id;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getTask_description() {
        return task_description;
    }

    public void setTask_description(String task_description) {
        this.task_description = task_description;
    }

    public LocalTime getCheck_In() {
        return check_In;
    }

    public void setCheck_In(LocalTime check_In) {
        this.check_In = check_In;
    }

    public LocalTime getCheck_out() {
        return check_out;
    }

    public void setCheck_out(LocalTime check_out) {
        this.check_out = check_out;
    }

    public Boolean getHalfDay() {
        return halfDay;
    }

    public void setHalfDay(Boolean halfDay) {
        this.halfDay = halfDay;
    }
}
