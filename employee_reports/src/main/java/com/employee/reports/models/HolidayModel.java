package com.employee.reports.models;

import javax.persistence.Column;
import java.time.LocalDate;

public class HolidayModel {
    private Integer id;
    private Integer empId;
    private LocalDate date;
    private String reasons;

    public HolidayModel() {
    }

    public HolidayModel(Integer id, Integer empId, LocalDate date, String reasons) {
        this.id = id;
        this.empId = empId;
        this.date = date;
        this.reasons = reasons;
    }

    public Integer getId() {
        return id;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    @Override
    public String toString() {
        return "HolidayModel{" +
                "id=" + id +
                ", empId=" + empId +
                ", date=" + date +
                ", reasons='" + reasons + '\'' +
                '}';
    }
}

