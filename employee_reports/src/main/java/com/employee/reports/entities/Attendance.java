package com.employee.reports.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "Attendance")
public class Attendance {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "Employee_id")
    private Integer eid;
    @Column(name = "A_date")
    private LocalDate date;
    @Column(name = "Task_description")
    private String task_description;
    @Column(name = "check_in")
    private LocalTime check_In;
    @Column(name = "check_out")
    private LocalTime check_out;
    @Column(name = "halfday")
    private Boolean halfDay;

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getTask_description() {
        return task_description;
    }

    public void setTask_description(String task_description) {
        this.task_description = task_description;
    }

    public LocalTime getCheck_In() {
        return check_In;
    }

    public void setCheck_In(LocalTime check_In) {
        this.check_In = check_In;
    }

    public LocalTime getCheck_out() {
        return check_out;
    }

    public void setCheck_out(LocalTime check_out) {
        this.check_out = check_out;
    }

    public Boolean getHalfDay() {
        return halfDay;
    }

    public void setHalfDay(Boolean halfDay) {
        this.halfDay = halfDay;
    }
}
