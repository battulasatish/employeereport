package com.employee.reports.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "Employee")
public class Employee {
    @Id
    @Column(name="Employee_id")
    private Integer id;
    @Column(name="Employee_fname")
    private String firstName;
    @Column(name="Employee_lname")
    private String lastName;
    @Column(name="Employee_DOJ")
    private LocalDate DOJ;
    @Column(name="Employee_Tmonths")
    private Integer tMonths;
    @Column(name="Employee_Tsalary")
    private Double tSal;
    @Column(name="Employee_salary")
    private Double sal;
    @Column(name="Employee_AHolidays")
    private Integer a_holidays;
    @Column(name="employee_prohibition")
    private Boolean prohibition;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDOJ() {
        return DOJ;
    }

    public void setDOJ(LocalDate DOJ) {
        this.DOJ = DOJ;
    }

    public Integer gettMonths() {
        return tMonths;
    }

    public void settMonths(Integer tMonths) {
        this.tMonths = tMonths;
    }

    public Double gettSal() {
        return tSal;
    }

    public void settSal(Double tSal) {
        this.tSal = tSal;
    }

    public Double getSal() {
        return sal;
    }

    public void setSal(Double sal) {
        this.sal = sal;
    }

    public Integer getA_holidays() {
        return a_holidays;
    }

    public void setA_holidays(Integer a_holidays) {
        this.a_holidays = a_holidays;
    }

    public Boolean getProhibition() {
        return prohibition;
    }

    public void setProhibition(Boolean prohibition) {
        this.prohibition = prohibition;
    }
}
