package com.employee.reports.repositories;

import com.employee.reports.entities.Employee;
import com.employee.reports.entities.Holidays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
    @Query(value = "select * from employee where year(Employee_DOJ)<=?", nativeQuery = true)
    List<Employee> employeeReport(Integer year);
}
