package com.employee.reports.repositories;

import com.employee.reports.entities.Attendance;
import com.employee.reports.entities.Holidays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance,Integer>{
    @Query(value = "select * from Attendance where a_date=?",nativeQuery = true)
    List<Attendance> taskOfTheDay(LocalDate date);
    @Query(value = "select * from Attendance where a_date=?1 and Employee_id=?2",nativeQuery = true)
    Attendance attendance(LocalDate date, Integer eid);
}
