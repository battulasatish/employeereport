package com.employee.reports.repositories;

import com.employee.reports.entities.Holidays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

@Repository
public interface HolidaysRepository extends JpaRepository<Holidays, Integer> {
    @Query(value = "select * from holidays where Month(h_date)<=?1 and year(h_date)=?2", nativeQuery = true)
    List<Holidays> reportOfMonth(Integer month,Integer year);

    @Query(value = "select * from holidays where year(h_date)=?", nativeQuery = true)
    List<Holidays> reportOfYear(Integer year);
     @Query(value = "select * from holidays where h_date=?1 and emp_id=?2",nativeQuery = true)
    Holidays existHoliday(LocalDate date,Integer empid);
}
