package com.employee.reports.service;

import com.employee.reports.entities.Attendance;
import com.employee.reports.models.AttendanceModel;
import com.employee.reports.models.EmployeeModel;
import com.employee.reports.models.HolidayModel;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface ReportsInterface {
    List<EmployeeModel> reportsOfMonth(Integer month,Integer year);
    List<EmployeeModel> reportsOfYear(Integer year);
    Map<String,Integer> taskOfTheDay(LocalDate date);
    String requestHoliday(HolidayModel holidayModel);
    String attendance(AttendanceModel attendanceModel);
    ByteArrayInputStream excelFile();
}
