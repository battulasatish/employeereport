package com.employee.reports.service;

import com.employee.reports.entities.Attendance;
import com.employee.reports.entities.Employee;
import com.employee.reports.entities.Holidays;
import com.employee.reports.models.AttendanceModel;
import com.employee.reports.models.EmployeeModel;
import com.employee.reports.models.HolidayModel;
import com.employee.reports.utility.ExcelBuilder;
import com.employee.reports.utility.Utility;
import com.employee.reports.repositories.AttendanceRepository;
import com.employee.reports.repositories.EmployeeRepository;
import com.employee.reports.repositories.HolidaysRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReportsImplimentation implements ReportsInterface {
    @Autowired
    AttendanceRepository attendanceRepository;
    @Autowired
    HolidaysRepository holidaysRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    Utility utility;

    @Override
    public List<EmployeeModel> reportsOfMonth(Integer month, Integer year) {
        List<Holidays> holidaysOfMont = holidaysRepository.reportOfMonth(month, year);
        Map<Integer, Long> employeeHolidayCount = holidaysOfMont.stream().map(Holidays::getEmpId).collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));
        List<Employee> employeeList = employeeRepository.employeeReport(year);
        List<EmployeeModel> employeeModels = new ArrayList<>();
        for (Employee employee : employeeList) {
            EmployeeModel e;
            e = utility.reportOfEmployee(employee.getId(), employeeHolidayCount);
            employeeModels.add(e);
        }
        return employeeModels;
    }

    @Override
    public List<EmployeeModel> reportsOfYear(Integer year) {
        List<Holidays> holidaysOfMont = holidaysRepository.reportOfYear(year);
        Map<Integer, Long> employeeHolidayCount = holidaysOfMont.stream().map(Holidays::getEmpId).collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));
        List<Employee> employeeList = employeeRepository.employeeReport(year);
        List<EmployeeModel> employeeModels = new ArrayList<>();
        for (Employee employee : employeeList) {
            EmployeeModel e;
            e = utility.reportOfyear(employee.getId(), employeeHolidayCount);
            e.setSalary(e.getSalary());
            employeeModels.add(e);
        }
        return employeeModels;
    }

    @Override
    public Map<String, Integer> taskOfTheDay(LocalDate date) {
        List<Attendance> task = attendanceRepository.taskOfTheDay(date);
        Map<String, Integer> taskMap = new TreeMap<>();
        task.forEach(e -> taskMap.put(e.getTask_description(), e.getEid()));
        return taskMap;
    }

    @Override
    public String requestHoliday(HolidayModel holidayModel) {
        if (holidaysRepository.existHoliday(holidayModel.getDate(), holidayModel.getEmpId()) != null || attendanceRepository.attendance(holidayModel.getDate(), holidayModel.getEmpId()) != null || holidayModel.getEmpId() == null) {
            return "holiday cannot be approved please contact superiors";
        } else {
            Holidays holiday = new Holidays();
            BeanUtils.copyProperties(holidayModel, holiday);
            holidaysRepository.save(holiday);
            return "holiday request has been forwarded";
        }
    }

    @Override
    public String attendance(AttendanceModel attendanceModel) {
        if (holidaysRepository.existHoliday(attendanceModel.getDate(), attendanceModel.getEid()) != null || attendanceRepository.attendance(attendanceModel.getDate(), attendanceModel.getEid()) != null || attendanceModel.getEid() == null) {
            return "internal error please contact superiors";
        } else {
            Attendance attendance = new Attendance();
            BeanUtils.copyProperties(attendanceModel, attendance);
            attendanceRepository.save(attendance);
            return "your attendance has been added";
        }
    }

    @Override
    public ByteArrayInputStream excelFile() {
        return new ExcelBuilder().build();
    }


}