package com.employee.reports.controller;

import com.employee.reports.models.AttendanceModel;
import com.employee.reports.models.EmployeeModel;
import com.employee.reports.models.HolidayModel;
import com.employee.reports.service.ReportsInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
public class EmployeeApi {
    @Autowired
    ReportsInterface reports;

    @GetMapping("/monthly/{month}/{year}")
    public List<EmployeeModel> monthlyReport(@PathVariable Integer month, @PathVariable Integer year) {
        return reports.reportsOfMonth(month, year);
    }

    @GetMapping("/yearly{year}")
    public List<EmployeeModel> yearlyReport(@PathVariable Integer year) {
        return reports.reportsOfYear(year);
    }

    @GetMapping("/taskForDay/{date}")
    public Map<String, Integer> taskOfTheDay(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return reports.taskOfTheDay(date);
    }

    @PostMapping("/holiday")
    public String requestHoliday(@RequestBody HolidayModel holidayModel) {
        return reports.requestHoliday(holidayModel);
    }

    @PostMapping("/attendance")
    public String attendance(@RequestBody AttendanceModel attendanceModel) {
        return reports.attendance(attendanceModel);
    }

    @GetMapping("/invoice")
    public ResponseEntity<Resource> excelFile() {
        String filename = "CJSSInvoice.xlsx";
        InputStreamResource file = new InputStreamResource(reports.excelFile());
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename).contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
    }
}
