package com.employee.reports.utility;

import com.employee.reports.entities.Employee;
import com.employee.reports.models.EmployeeModel;
import com.employee.reports.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class Utility {
    @Autowired
    EmployeeRepository employeeRepository;

    public EmployeeModel reportOfEmployee(Integer i, Map<Integer, Long> m) {
        Employee employee = employeeRepository.getById(i);
        EmployeeModel e = new EmployeeModel();
        if (employee.getProhibition()) {
            if (m.containsKey(employee.getId())) {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                Integer holidaysCount = m.get(employee.getId()).intValue();
                e.setNumberOfHolidays(holidaysCount);
                if (holidaysCount > employee.getA_holidays()) {
                    Double salary =
                            employee.gettSal() - (holidaysCount - employee.getA_holidays()) * employee.gettSal() / 30;
                    e.setSalary(salary);
                } else {
                    e.setSalary(employee.gettSal());
                }
            } else {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                e.setNumberOfHolidays(0);
                e.setSalary(employee.gettSal());
            }
        } else {
            if (m.containsKey(employee.getId())) {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                Integer holidaysCount = m.get(employee.getId()).intValue();
                e.setNumberOfHolidays(holidaysCount);
                if (holidaysCount > employee.getA_holidays()) {
                    Double salary =
                            employee.getSal() - (holidaysCount - employee.getA_holidays()) * employee.getSal() / 30;
                    e.setSalary(salary);
                } else {
                    e.setSalary(employee.getSal());
                }
            } else {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                e.setNumberOfHolidays(0);
                e.setSalary(employee.getSal());
            }
        }
        return e;
    }

    public EmployeeModel reportOfyear(Integer i, Map<Integer, Long> m) {
        Employee employee = employeeRepository.getById(i);
        EmployeeModel e = new EmployeeModel();
        if (employee.getProhibition()) {
            if (m.containsKey(employee.getId())) {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                Integer holidaysCount = m.get(employee.getId()).intValue();
                e.setNumberOfHolidays(holidaysCount);
                if (holidaysCount > employee.getA_holidays()) {
                    Double salary =
                            (employee.gettMonths() * employee.gettSal()) + ((12 - employee.gettMonths()) * employee.getSal())
                                    - (holidaysCount - employee.getA_holidays()) * employee.gettSal() / 30;
                    e.setSalary(salary);
                } else {
                    e.setSalary((employee.gettMonths() * employee.gettSal()) + ((12 - employee.gettMonths()) * employee.getSal()));
                }
            } else {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                e.setNumberOfHolidays(0);
                e.setSalary((employee.gettMonths() * employee.gettSal()) + ((12 - employee.gettMonths()) * employee.getSal()));
            }
        } else {
            if (m.containsKey(employee.getId())) {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                Integer holidaysCount = m.get(employee.getId()).intValue();
                e.setNumberOfHolidays(holidaysCount);
                if (holidaysCount > employee.getA_holidays()) {
                    Double salary =
                            12 * employee.getSal() - (holidaysCount - employee.getA_holidays()) * employee.getSal() / 30;
                    e.setSalary(salary);
                } else {
                    e.setSalary(12 * employee.getSal());
                }
            } else {
                e.setId(employee.getId());
                e.setFirstName(employee.getFirstName());
                e.setLastName(employee.getLastName());
                e.setNumberOfHolidays(0);
                e.setSalary(12 * employee.getSal());
            }
        }
        return e;
    }
}