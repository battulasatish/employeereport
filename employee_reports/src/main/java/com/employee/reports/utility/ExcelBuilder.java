package com.employee.reports.utility;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Random;

public class ExcelBuilder {
    ExcelHelper excelHelper = new ExcelHelper();
    XSSFWorkbook workbook = excelHelper.workbook;
    XSSFSheet sheet = excelHelper.sheet;

    public ByteArrayInputStream build() {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            //creating a sheet for work book
            //cjss row
            Row firstrow = sheet.createRow(0);
            //cjss cell
            Cell frowfcell = firstrow.createCell(0);
            frowfcell.setCellValue("CJSS Technologies Private Limited ");
            frowfcell.setCellStyle(excelHelper.headingstyle());
            sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 10));
            //address,gst and original row
            Row secondrow = sheet.createRow(3);
            sheet.addMergedRegion(new CellRangeAddress(3, 5, 0, 8));
            Cell srfcell = secondrow.createCell(0);
            srfcell.setCellStyle(excelHelper.subtitle());
            srfcell.setCellValue("address: gachibowli,hyderabad \n GST No.");
            Cell srscell = secondrow.createCell(9);
            srscell.setCellValue("0riginal");
            srscell.setCellStyle(excelHelper.subtitle());
            sheet.addMergedRegion(new CellRangeAddress(3, 5, 9, 10));
            //empty row
            Row emptyRow = sheet.createRow(6);
            sheet.addMergedRegion(new CellRangeAddress(6, 6, 0, 10));
            emptyRow.createCell(0).setCellValue("");
            //after empty row description
            Row thirdRow = sheet.createRow(7);
            sheet.addMergedRegion(new CellRangeAddress(7, 7, 0, 10));
            Cell thirdRowCell = thirdRow.createCell(0);
            thirdRowCell.setCellValue("EXPORT SUPPLY OF SERVICE WITHOUT PAYMENT OF IGST");
            thirdRowCell.setCellStyle(excelHelper.centerData());
            //Invoice row
            Row fourthRow = sheet.createRow(8);
            sheet.addMergedRegion(new CellRangeAddress(8, 8, 0, 10));
            Cell fourthRowCell = fourthRow.createCell(0);
            fourthRowCell.setCellValue("INVOICE");
            fourthRowCell.setCellStyle(excelHelper.invoice());
            // invoice no and date row
            Row fifthRow = sheet.createRow(9);
            sheet.addMergedRegion(new CellRangeAddress(9, 10, 0, 6));
            Cell fifthRowFirstCell = fifthRow.createCell(0);
            fifthRowFirstCell.setCellValue("Invoice No: 01/A1/2022\nInvoice date:" + LocalDate.now());
            fifthRowFirstCell.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(9, 9, 7, 10));
            Cell fifthRowsidecell1 = fifthRow.createCell(7);
            fifthRowsidecell1.setCellValue("");
            //empty rows beside invoice one added above and second row below
            Row fifthRowEmpty = sheet.createRow(10);
            sheet.addMergedRegion(new CellRangeAddress(10, 10, 7, 10));
            Cell emptyDatecell = fifthRowEmpty.createCell(7);
            emptyDatecell.setCellValue("");
            //Reverse charge row
            Row sixthRow = sheet.createRow(11);
            sheet.addMergedRegion(new CellRangeAddress(11, 11, 0, 4));
            Cell sixthRowFirstCell = sixthRow.createCell(0);
            sixthRowFirstCell.setCellStyle(excelHelper.sideDate());
            sixthRowFirstCell.setCellValue("Reverse Charge(Y/N):");
            sheet.addMergedRegion((new CellRangeAddress(11, 11, 5, 6)));
            Cell sixthRowSecondCell = sixthRow.createCell(5);
            sixthRowSecondCell.setCellValue("N");
            sheet.addMergedRegion(new CellRangeAddress(11, 11, 7, 10));
            Cell sixthRowThirdCell = sixthRow.createCell(7);
            sixthRowThirdCell.setCellValue("");

            //State and code row
            Row seventhRow = sheet.createRow(12);
            sheet.addMergedRegion(new CellRangeAddress(12, 12, 0, 3));
            Cell seventhRowFirstCell = seventhRow.createCell(0);
            seventhRowFirstCell.setCellStyle(excelHelper.sideDate());
            seventhRowFirstCell.setCellValue("State");
            Cell seventhRowSecondCell = seventhRow.createCell(4);
            seventhRowSecondCell.setCellValue("Code");
            seventhRowSecondCell.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(12, 12, 5, 6));
            seventhRow.createCell(5).setCellValue("");
            sheet.addMergedRegion(new CellRangeAddress(12, 12, 7, 10));
            seventhRow.createCell(7).setCellValue("");

            //After state empty row
            Row breakRow2 = sheet.createRow(13);
            sheet.addMergedRegion(new CellRangeAddress(13, 13, 0, 10));
            breakRow2.createCell(0).setCellValue("");

            //party details heading
            Row eighthRow = sheet.createRow(14);
            sheet.addMergedRegion(new CellRangeAddress(14, 14, 0, 6));
            Cell eighthRowFirstcell = eighthRow.createCell(0);
            eighthRowFirstcell.setCellStyle(excelHelper.subHeadings());
            eighthRowFirstcell.setCellValue("Name:       Bill to Party");
            sheet.addMergedRegion(new CellRangeAddress(14, 14, 7, 10));
            Cell eighthRowSecondCell = eighthRow.createCell(7);
            eighthRowSecondCell.setCellValue("ship to party");
            eighthRowSecondCell.setCellStyle(excelHelper.subHeadings());

            //pte.ltd Row of client
            Row ninthRow = sheet.createRow(15);
            sheet.addMergedRegion(new CellRangeAddress(15, 16, 0, 6));
            Cell ninthRowFirstcell = ninthRow.createCell(0);
            ninthRowFirstcell.setCellStyle(excelHelper.sideDate());
            ninthRowFirstcell.setCellValue("Address:");
            sheet.addMergedRegion(new CellRangeAddress(15, 16, 7, 10));
            ninthRow.createCell(7).setCellValue("");

            //Address row of client
            Row tenthRow = sheet.createRow(17);
            sheet.addMergedRegion(new CellRangeAddress(17, 17, 0, 6));
            Cell tenthRowFirstcell = tenthRow.createCell(0);
            tenthRowFirstcell.setCellStyle(excelHelper.sideDate());
            tenthRowFirstcell.setCellValue("Pte.Ltd");
            sheet.addMergedRegion(new CellRangeAddress(17, 17, 7, 10));
            tenthRow.createCell(7).setCellValue("");

            //purchase order number row
            Row eleventhRow = sheet.createRow(18);
            sheet.addMergedRegion(new CellRangeAddress(18, 18, 0, 6));
            Cell eleventhRowFirstcell = eleventhRow.createCell(0);
            eleventhRowFirstcell.setCellStyle(excelHelper.sideDate());
            eleventhRowFirstcell.setCellValue("Order Number:      " + new Random().nextInt(1000000000));
            sheet.addMergedRegion(new CellRangeAddress(18, 18, 7, 10));
            eleventhRow.createCell(7).setCellValue("");

            //break row 2 after client details
            Row twelfthRow = sheet.createRow(19);
            sheet.addMergedRegion((new CellRangeAddress(19, 19, 0, 10)));
            twelfthRow.createCell(0).setCellValue("");

            // dynamic pricing table heading row
            Row thirteenth = sheet.createRow(20);
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 0, 0));
            Cell thirteenthFirst = thirteenth.createCell(0);
            thirteenthFirst.setCellValue("S.NO");
            thirteenthFirst.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 1, 1));
            Cell thirteenthSecond = thirteenth.createCell(1);
            thirteenthSecond.setCellValue("Service\nDescription");
            thirteenthSecond.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 2, 2));
            Cell thirteenthThird = thirteenth.createCell(2);
            thirteenthThird.setCellValue("SAC");
            thirteenthThird.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 3, 3));
            Cell thirteenthFourth = thirteenth.createCell(3);
            thirteenthFourth.setCellValue("From\nDate");
            thirteenthFourth.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 4, 4));
            Cell thirteenthFifth = thirteenth.createCell(4);
            thirteenthFifth.setCellValue("To Date");
            thirteenthFifth.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 5, 5));
            Cell thirteenthsixth = thirteenth.createCell(5);
            thirteenthsixth.setCellValue("Amount");
            thirteenthsixth.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 6, 6));
            Cell thirteenthseventh = thirteenth.createCell(6);
            thirteenthseventh.setCellValue("Discount");
            thirteenthseventh.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 7, 7));
            Cell thirteentheighth = thirteenth.createCell(7);
            thirteentheighth.setCellValue("Taxable\nvalue");
            thirteentheighth.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(20, 20, 8, 9));
            Cell thirteenthninth = thirteenth.createCell(8);
            thirteenthninth.setCellValue("IGST");
            CellStyle topBorder = excelHelper.invoice();
            topBorder.setBorderTop(BorderStyle.THIN);
            thirteenthninth.setCellStyle(topBorder);
            sheet.addMergedRegion(new CellRangeAddress(20, 21, 10, 10));
            Cell thirteenthten = thirteenth.createCell(10);
            thirteenthten.setCellValue("Total");
            thirteenthten.setCellStyle(excelHelper.subHeadings());

            //IGST subvalues in payment details table
            Row fourteen = sheet.createRow(21);
            Cell fourteenFirst = fourteen.createCell(8);
            fourteenFirst.setCellStyle(excelHelper.subHeadings());
            fourteenFirst.setCellValue("Rate");
            Cell fourteenSecond = fourteen.createCell(9);
            fourteenSecond.setCellValue("Amount");
            fourteenSecond.setCellStyle(excelHelper.subHeadings());

            //initializing a variable for dynamic row creation
            int i = 22;
            int j = i + 2;
            //dynamic table dynamic data row
            Row fifteen = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, j, 0, 0));
            Cell fifteenFirst = fifteen.createCell(0);
            fifteenFirst.setCellStyle(excelHelper.dynamicData());
            fifteenFirst.setCellValue("1");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 1, 1));
            Cell fifteenSecond = fifteen.createCell(1);
            fifteenSecond.setCellStyle(excelHelper.dynamicData());
            fifteenSecond.setCellValue("Performance Testing");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 2, 2));
            Cell fifteenThird = fifteen.createCell(2);
            fifteenThird.setCellStyle(excelHelper.dynamicData());
            fifteenThird.setCellValue("998314");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 3, 3));
            Cell fifteenFourth = fifteen.createCell(3);
            fifteenFourth.setCellStyle(excelHelper.dynamicData());
            fifteenFourth.setCellValue("01-11-2022");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 4, 4));
            Cell fifteenFifth = fifteen.createCell(4);
            fifteenFifth.setCellStyle(excelHelper.dynamicData());
            fifteenFifth.setCellValue("30-11-2022");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 5, 5));
            Cell fifteenSixth = fifteen.createCell(5);
            fifteenSixth.setCellStyle(excelHelper.dynamicData());
            fifteenSixth.setCellValue("123");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 6, 6));
            Cell fifteenSeventh = fifteen.createCell(6);
            fifteenSeventh.setCellStyle(excelHelper.dynamicData());
            fifteenSeventh.setCellValue("");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 7, 7));
            Cell fifteenEighth = fifteen.createCell(7);
            fifteenEighth.setCellStyle(excelHelper.dynamicData());
            fifteenEighth.setCellValue("");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 8, 8));
            Cell fifteenNinth = fifteen.createCell(8);
            fifteenNinth.setCellStyle(excelHelper.dynamicData());
            fifteenNinth.setCellValue("");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 9, 9));
            Cell fifteenTenth = fifteen.createCell(9);
            fifteenTenth.setCellStyle(excelHelper.dynamicData());
            fifteenTenth.setCellValue("0");
            sheet.addMergedRegion(new CellRangeAddress(i, j, 10, 10));
            Cell fifteenEleven = fifteen.createCell(10);
            fifteenEleven.setCellStyle(excelHelper.dynamicData());
            fifteenEleven.setCellValue("123");

            // after looping completed i= intial row before and j is ending row
            // break after dynamic data
            //add one more row to increase value of j to add one more row
            j = j + 1;
            Row breakRow = sheet.createRow(j);
            sheet.addMergedRegion(new CellRangeAddress(j, j + 1, 0, 10));
            breakRow.createCell(0).setCellValue("");
            // added one more row to j and assigned i=j+1 so merged row
            j = j + 2;
            i = j + 1;
            Row sixteen = sheet.createRow(j);
            sheet.addMergedRegion(new CellRangeAddress(j, i, 0, 2));
            Cell sixreenFirst = sixteen.createCell(0);
            sixreenFirst.setCellValue("Total");
            sixreenFirst.setCellStyle(excelHelper.invoice());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 3, 3));
            Cell sixteenSecond = sixteen.createCell(3);
            sixteenSecond.setCellValue("0");
            sixteenSecond.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 4, 4));
            Cell sixteenThird = sixteen.createCell(4);
            sixteenThird.setCellValue("");
            sixteenThird.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 5, 5));
            Cell sixteenFourth = sixteen.createCell(5);
            sixteenFourth.setCellValue("123");
            sixteenFourth.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 6, 6));
            Cell sixteenFifth = sixteen.createCell(6);
            sixteenFifth.setCellValue("0");
            sixteenFifth.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 7, 7));
            Cell sixteenSixth = sixteen.createCell(7);
            sixteenSixth.setCellValue("0");
            sixteenSixth.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 8, 8));
            Cell sixteenSeventh = sixteen.createCell(8);
            sixteenSeventh.setCellValue("");
            sixteenSeventh.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 9, 9));
            Cell sixteenEighth = sixteen.createCell(9);
            sixteenEighth.setCellValue("0");
            sixteenEighth.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, i, 10, 10));
            Cell sixteenNinth = sixteen.createCell(10);
            sixteenNinth.setCellValue("0");
            sixteenNinth.setCellStyle(excelHelper.sideDate());
            //merged row i created here
            Row newRow = sheet.createRow(i);
            newRow.createCell(0).setCellValue("");

            //total in words row adding j two value so that before i is j+1 so now j=i+1
            j = i + 1;
            Row seventeen = sheet.createRow(j);
            sheet.addMergedRegion(new CellRangeAddress(j, j, 0, 6));
            Cell seventeenFirst = seventeen.createCell(0);
            seventeenFirst.setCellValue("Total Invoice Amount In words");
            seventeenFirst.setCellStyle(excelHelper.subHeadings());
            sheet.addMergedRegion(new CellRangeAddress(j, j, 7, 9));
            Cell seventeenSecond = seventeen.createCell(7);
            seventeenSecond.setCellStyle(excelHelper.subtitle());
            seventeenSecond.setCellValue("Total Amount Before Tax");
            Cell seventeenThird = seventeen.createCell(10);
            seventeenThird.setCellValue("SGD 123");
            seventeenThird.setCellStyle(excelHelper.sideDate());

            //filling amount in words added one more value to j=j+1 and i= new j+1=30 static
            j = j + 1;
            i = j + 1;
            Row eighteen = sheet.createRow(j);
            sheet.addMergedRegion(new CellRangeAddress(j, i, 0, 6));
            Cell eighteenFirst = eighteen.createCell(0);
            eighteenFirst.setCellValue("");
            eighteenFirst.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(j, j, 7, 9));
            Cell eighteenSecond = eighteen.createCell(7);
            eighteenSecond.setCellStyle(excelHelper.sideDate());
            eighteenSecond.setCellValue("Add : IGST");
            Cell eighteenThird = eighteen.createCell(10);
            eighteenThird.setCellValue("");
            eighteenThird.setCellStyle(excelHelper.sideDate());

            //Total Amount after Tax row here already i  initialized for this row i=31 here in static
            Row nineteen = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, i, 7, 9));
            Cell nineteenFirst = nineteen.createCell(7);
            nineteenFirst.setCellStyle(excelHelper.sideDate());
            nineteenFirst.setCellValue("Total Amount After Tax");
            Cell nineteenSecond = nineteen.createCell(10);
            nineteenSecond.setCellValue("SGD 123");
            nineteenSecond.setCellStyle(excelHelper.sideDate());

            //Bank details row j=30, i=31 before this
            i = i + 1;
            Row twenty = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, i, 0, 3));
            Cell twentyFirst = twenty.createCell(0);
            twentyFirst.setCellStyle(excelHelper.subHeadings());
            twentyFirst.setCellValue("Bank details");

            // stamp seal cell merging ith row and 7 more total 8 rows
            sheet.addMergedRegion(new CellRangeAddress(i, i + 7, 4, 6));
            Cell stampmerged = twenty.createCell(4);
            stampmerged.setCellValue("");
            sheet.addMergedRegion(new CellRangeAddress(i, i, 7, 9));
            Cell twentyThird = twenty.createCell(7);
            twentyThird.setCellValue("GST on Reverse Charge");
            twentyThird.setCellStyle(excelHelper.subHeadings());
            Cell twentyFourth = twenty.createCell(10);
            twentyFourth.setCellStyle(excelHelper.sideDate());
            twentyFourth.setCellValue("0");

            // HDFC ROW AND ADDRESS i=32 before this
            i = i + 1;
            Row twentyOne = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, i + 1, 0, 3)); // one more used
            Cell twentyOneFirst = twentyOne.createCell(0);
            twentyOneFirst.setCellValue("HDFC Bank\n Address");
            twentyOneFirst.setCellStyle(excelHelper.sideDate());
            sheet.addMergedRegion(new CellRangeAddress(i, i + 1, 7, 10));
            Cell twentyOneSecond = twentyOne.createCell(7);
            twentyOneSecond.setCellValue("");

            //i=33 before so need to add 2 more rows
            i = i + 2;
            Row twentyTwo = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, i, 0, 3));
            Cell twentyTwoFirst = twentyTwo.createCell(0);
            twentyTwoFirst.setCellValue("Bank  A/C:");
            twentyTwoFirst.setCellStyle(excelHelper.sideDate());
            // sign merged region
            sheet.addMergedRegion(new CellRangeAddress(i, i + 4, 7, 10));
            Cell signmerged = twentyTwo.createCell(7);
            signmerged.setCellStyle(excelHelper.sideDate());
            signmerged.setCellValue("Certified the particulars given above    are true and correct\n" +
                    "       For CJSS Technologies Private Limited");

            //Bank IFSC row i =35 before
            i = i + 1;
            Row twentyThree = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, i, 0, 3));
            Cell twentyThreeFirst = twentyThree.createCell(0);
            twentyThreeFirst.setCellValue("Bank IFSC:");
            twentyThreeFirst.setCellStyle(excelHelper.sideDate());

            //declaration merged area i= 36 before
            i = i + 1;
            Row twentyFour = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, i + 3, 0, 3));
            Cell twentyFourFirst = twentyFour.createCell(0);
            twentyFourFirst.setCellValue("Declaration: \nWE DECLARE THAT THIS IS INVOICE    SHOWS " +
                    "THE ACTUAL PRICE OF THE GOODS DESCRIBED " +
                    "AND THAT ALL\nPARTICULARS ARE TRUE AND CORRECT");
            twentyFourFirst.setCellStyle(excelHelper.sideDate());

            //last row that common seal and Authorized signature i=37 before
            i = i + 3;
            Row twentyFive = sheet.createRow(i);
            sheet.addMergedRegion(new CellRangeAddress(i, i, 4, 6));
            Cell twentyFiveFirst = twentyFive.createCell(4);
            twentyFiveFirst.setCellStyle(excelHelper.centerData());
            twentyFiveFirst.setCellValue("Common Seal");
            sheet.addMergedRegion(new CellRangeAddress(i, i, 7, 10));
            Cell twentyFiveSecond = twentyFive.createCell(7);
            twentyFiveSecond.setCellValue("Authorised Signatory");
            twentyFiveSecond.setCellStyle(excelHelper.centerData());

            //writing to the byte stream
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException exception) {

            return null;
        }
    }

}