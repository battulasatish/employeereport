package com.employee.reports.utility;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;

public class ExcelHelper {
    public  XSSFWorkbook workbook = new XSSFWorkbook();
    public XSSFSheet sheet = workbook.createSheet("invoice");
    public  CellStyle headingstyle(){
        Font headingfont = workbook.createFont();
        headingfont.setBold(true);
        headingfont.setFontHeightInPoints((short) 22);
        CellStyle style= workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFont(headingfont);
        style.setWrapText(true);
        style.setBorderBottom(BorderStyle.NONE);
        return style;
    }
    public CellStyle subtitle(){
        //no colour headings
        Font subfont = workbook.createFont();
        subfont.setBold(true);
       subfont.setFontHeightInPoints((short) 12);
        CellStyle style= workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        style.setFont(subfont);
        style.setWrapText(true);
        style.setBorderRight(BorderStyle.NONE);
        return style;

    }
    public CellStyle centerData(){
        Font font = workbook.createFont();
        font.setBold(true);
        font.setItalic(true);
        font.setFontHeightInPoints((short) 10);
        CellStyle style= workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        style.setFont(font);
        style.setWrapText(true);
        return style;
    }
    public CellStyle invoice(){
        Font headingfont = workbook.createFont();
        headingfont.setBold(true);
        headingfont.setFontHeightInPoints((short) 15);
        CellStyle style= workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.TOP);
        style.setFont(headingfont);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setWrapText(true);
        style.setBorderBottom(BorderStyle.NONE);
        return style;
    }
    public CellStyle subHeadings(){
        Font headingfont = workbook.createFont();
        headingfont.setBold(true);
        headingfont.setFontHeightInPoints((short) 12);
        CellStyle style=invoice();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setFont(headingfont);
        return  style;
    }
    public CellStyle sideDate(){
        CellStyle style=centerData();
        style.setAlignment(HorizontalAlignment.JUSTIFY);
        style.setWrapText(true);
        return style;
    }
    public CellStyle dynamicData(){
        CellStyle style=sideDate();
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return style;
    }

}
